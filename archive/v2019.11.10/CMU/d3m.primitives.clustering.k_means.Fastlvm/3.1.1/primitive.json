{
    "id": "66c3bb07-63f7-409e-9f0f-5b07fbf7cd8e",
    "version": "3.1.1",
    "name": "K-means Clustering",
    "description": "This class provides functionality for unsupervised clustering, which according to Wikipedia is 'the task of\ngrouping a set of objects in such a way that objects in the same group (called a cluster) are more similar to\neach other than to those in other groups'. It is a main task of exploratory data mining, and a common technique\nfor statistical data analysis. The similarity measure can be, in general, any metric measure: standard Euclidean\ndistance is the most common choice and the one currently implemented. In future, adding other metrics should not\nbe too difficult. Standard packages, like those in scikit learn run on a single machine and often only on one\nthread. Whereas our underlying C++ implementation can be distributed to run on multiple machines. To enable the\ndistribution through python interface is work in progress. In this class, we implement a K-Means clustering using\nLlyod's algorithm and speed-up using Cover Trees. The API is similar to sklearn.cluster.KMeans. The class is\npickle-able.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "python_path": "d3m.primitives.clustering.k_means.Fastlvm",
    "primitive_family": "CLUSTERING",
    "algorithm_types": [
        "K_MEANS_CLUSTERING"
    ],
    "keywords": [
        "large scale K-Means",
        "clustering"
    ],
    "source": {
        "name": "CMU",
        "contact": "mailto:donghanw@cs.cmu.edu",
        "uris": [
            "https://gitlab.datadrivendiscovery.org/cmu/fastlvm",
            "https://github.com/autonlab/fastlvm"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/autonlab/fastlvm.git@98f1006b205cc6e556a48ca0d1bf26a56e54b0d9#egg=fastlvm"
        }
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "fastlvm.kmeans.KMeans",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "fastlvm.kmeans.Params",
            "Hyperparams": "fastlvm.kmeans.HyperParams"
        },
        "interfaces_version": "2019.11.10",
        "interfaces": [
            "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "k": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 10,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "The number of clusters to form as well as the number of centroids to generate.",
                "lower": 1,
                "upper": 10000,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "iters": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 100,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "The number of iterations of the Lloyd\u2019s algorithm for K-Means clustering.",
                "lower": 1,
                "upper": 10000,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "initialization": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "kmeanspp",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "'random': choose k observations (rows) at random from data for the initial centroids. 'kmeanspp' : selects initial cluster centers by finding well spread out points using cover trees to speed up convergence. 'covertree' : selects initial cluster centers by sampling to speed up convergence.",
                "values": [
                    "random",
                    "firstk",
                    "kmeanspp",
                    "covertree"
                ]
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "fastlvm.kmeans.HyperParams",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "params": {
                "type": "fastlvm.kmeans.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams"
                ],
                "returns": "NoneType"
            },
            "evaluate": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "float",
                "description": "Finds the score of learned model on a set of test points\n\nParameters\n----------\ninputs : Inputs\n    A NxD matrix of data points.\n\nReturns\n-------\nscore : float\n    The score (-ve of K-Means objective value) on the supplied points."
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Compute k-means clustering\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_call_metadata": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "bool",
                "description": "Returns metadata about the last ``fit`` call if it succeeded\n\nReturns\n-------\nStatus : bool\n    True/false status of fitting."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "fastlvm.kmeans.Params",
                "description": "Get parameters of KMeans.\n\nParameters are basically the cluster centres in byte stream.\n\nReturns\n-------\nparams : Params\n    A named tuple of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Finds the closest cluster for the given set of test points using the learned model.\n\nParameters\n----------\ninputs : Inputs\n    A NxD matrix of data points.\n\nReturns\n-------\nOutputs\n    The index of the cluster each sample belongs to."
            },
            "produce_centers": {
                "kind": "PRODUCE",
                "arguments": [],
                "returns": "d3m.container.numpy.ndarray",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Get current cluster centers for this model.\n\nReturns\n----------\ncenters : numpy.ndarray\n    A KxD matrix of cluster centres."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Set parameters of KMeans.\n\nParameters are basically the cluster centres in byte stream.\n\nParameters\n----------\nparams : Params\n    A named tuple of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "NoneType",
                "description": "Sets training data for KMeans.\n\nParameters\n----------\ntraining_inputs : Inputs\n    A NxD DataFrame of data points for training."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "cluster_centers": "bytes"
        }
    },
    "structural_type": "fastlvm.kmeans.KMeans",
    "digest": "96bde1ea597377d13e6807da590ad1b69e7902c914f5687c9eaaa74fcb6b32e8"
}
